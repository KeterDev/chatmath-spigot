package net.KeterDev.ChatMath;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Server;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.EventExecutor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.gson.JsonParser;

import net.KeterDev.ChatMath.Event.ChatListener;
import net.KeterDev.ChatMath.Event.CommandListener;
import net.KeterDev.ChatMath.Game.GuessTheNumber;
import net.KeterDev.ChatMath.Game.RateLimiter;
import net.KeterDev.ChatMath.Game.SolveExpression;

public class Main extends JavaPlugin {
	public static String pluginVersion;
	public static String actualVersion;

	public static Main instance;
	public static Server server;
	private static PluginManager pluginManager;
	private static File configFile;
	public static Logger logger;
	public static FileConfiguration config;
	public static int schedulerTaskID;
	public static String chatPrefix;
	public static int gameType;
	public static RateLimiter rl = new RateLimiter();

	private static final String USER_AGENT = "ChatMath/4.0";
	private static final String REQUEST_URL = "https://api.spigotmc.org/legacy/update.php?resource=49076";

	@Override
	public void onEnable() {
		try {
			pluginVersion = getDescription().getVersion();
			actualVersion = getVersion();

			saveDefaultConfig();
			instance = this;
			server = getServer();
			pluginManager = server.getPluginManager();
			configFile = new File(getDataFolder() + File.separator + "config.yml");
			logger = getLogger();

			if (!configFile.exists()) {
				saveDefaultConfig();
			}
			config = getConfig();
			chatPrefix = config.getConfigurationSection("Messages").getString("Prefix");

			Listener listener = new ChatListener();
			EventExecutor executor = new EventExecutor() {

				@Override
				public void execute(Listener listener, Event event) throws EventException {
					ChatListener cListener = (ChatListener) listener;
					cListener.onPlayerChats((AsyncPlayerChatEvent) event);
				}
			};

			PluginCommand cmd = server.getPluginCommand("chatmath");
			cmd.setExecutor(new CommandListener());

			if (config.getBoolean("EventOverride", false)) {

				logger.log(Level.WARNING, "An event override mode was enabled in config.");
				logger.log(Level.WARNING, "Plugin will have the highest priority, but this may cause side effects.");

				server.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {

					@Override
					public void run() {
						pluginManager.registerEvent(AsyncPlayerChatEvent.class, listener, EventPriority.HIGHEST,
								executor, instance, true);
					}
				});
			} else {
				pluginManager.registerEvent(AsyncPlayerChatEvent.class, listener, EventPriority.NORMAL, executor, this);
			}

			if (config.getKeys(false).size() <= 1) {
				logger.log(Level.CONFIG, "Config seems to be empty, attempting to fix...");
				configFile.delete();
				saveDefaultConfig();
			}

			new Thread(new Runnable() {

				@Override
				public void run() {
					if (!actualVersion.equals(pluginVersion) && config.getBoolean("NotifyOnUpdate", false)) {
						logger.log(Level.INFO, "A new version has been released: " + actualVersion);
						File downloadFile = new File(getDataFolder() + File.separator + "updates" + File.separator
								+ "ChatMath-" + actualVersion + ".jar");
						if (config.getBoolean("AutoDownloadUpdate", false)) {
							if (!downloadFile.exists()) {
								logger.log(Level.INFO,
										"It will be automatically downloaded to plugins/ChatMath/updates/"
												+ downloadFile.getName());
								try {
									if (downloadUpdate().exists()) {
										logger.log(Level.INFO, "Download complete.");
									} else {
										logger.log(Level.INFO, "Download appears to be failed.");
									}
								} catch (IOException e) {
									File logFile = ExceptionLogger.logException(e);
									logger.log(Level.SEVERE,
											"Download failed. A detailed stacktrace is saved to ChatMath/errorlogs/"
													+ logFile.getName());
								}
							} else {
								logger.log(Level.INFO,
										"It's already downloaded to ChatMath/updates/" + downloadFile.getName());
							}
						} else {
							logger.log(Level.INFO,
									"Run /chatmath update to start download. (This command can be issued from the console only).");
						}
					}
				}
			}).run();
			logger.log(Level.INFO, "Challenge will be started when all plugins are enabled.");
			scheduleRestart();
		} catch (Exception e) {
			logError(e);
		}
	}

	public static String getVersion() throws IOException {
		URL url = new URL(REQUEST_URL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.addRequestProperty("User-Agent", USER_AGENT);

		InputStream inputStream = connection.getInputStream();
		InputStreamReader reader = new InputStreamReader(inputStream);

		return new JsonParser().parse(reader).getAsString();

	}

	public static File downloadUpdate() throws IOException {
		URL website = new URL(
				"https://bitbucket.org/KeterDev/chatmath-spigot/downloads/ChatMath-" + actualVersion + ".jar");
		File dest = new File(instance.getDataFolder() + File.separator + "updates" + File.separator + "ChatMath-"
				+ actualVersion + ".jar");
		ReadableByteChannel rbc = Channels.newChannel(website.openStream());
		FileOutputStream fos = new FileOutputStream(dest);
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		fos.flush();
		fos.close();
		return dest;
	}

	public static void scheduleRestart() {
		schedulerTaskID = Main.server.getScheduler().scheduleSyncRepeatingTask(Main.instance, new Runnable() {

			@Override
			public void run() {
				runGame();
			}
		}, 0, Main.config.getLong("Interval", 300) * 20);
	}

	public static void runGame() {
		Random rand = new Random();
		int r = rand.nextInt(2);
		if (r == 0) {
			gameType = r;
			SolveExpression.start();
		} else if (r == 1) {
			gameType = r;
			GuessTheNumber.start();
		}
	}

	public static void runGame(int id) {
		if (id == 0) {
			SolveExpression.start();
			gameType = id;
		} else if (id == 1) {
			GuessTheNumber.start();
			gameType = id;
		}
	}

	public static ConfigurationSection[] getSolveExpressionSections(FileConfiguration cfg) {
		try {
			ConfigurationSection expressionsRoot = cfg.getConfigurationSection("Expressions");
			List<ConfigurationSection> sections = new ArrayList<>();

			for (String key : expressionsRoot.getKeys(false)) {
				if (expressionsRoot.getConfigurationSection(key).getBoolean("Enabled", false)) {
					sections.add(expressionsRoot.getConfigurationSection(key));
				}
			}

			return sections.toArray(new ConfigurationSection[0]);
		} catch (NullPointerException e) {
			if (config.getKeys(false).size() <= 1) {
				logger.log(Level.CONFIG, "Config seems to be empty, attempting to fix...");
				configFile.delete();
				Main.instance.saveDefaultConfig();
			}

		} catch (Exception e) {
			logError(e);
		}
		return null;
	}

	public static ConfigurationSection[] getGuessTheNumberSections(FileConfiguration cfg) {
		try {
			ConfigurationSection expressionsRoot = cfg.getConfigurationSection("GuessNumber");
			List<ConfigurationSection> sections = new ArrayList<>();

			for (String key : expressionsRoot.getKeys(false)) {
				if (expressionsRoot.getConfigurationSection(key).getBoolean("Enabled", false)) {
					sections.add(expressionsRoot.getConfigurationSection(key));
				}
			}

			return sections.toArray(new ConfigurationSection[0]);
		} catch (NullPointerException e) {
			if (config.getKeys(false).size() <= 1) {
				logger.log(Level.CONFIG, "Config seems to be empty, attempting to fix...");
				configFile.delete();
				Main.instance.saveDefaultConfig();
			}

		} catch (Exception e) {
			logError(e);
		}
		return null;
	}

	public static FileConfiguration getReloadConfig() {
		instance.reloadConfig();
		instance.saveConfig();
		return YamlConfiguration.loadConfiguration(configFile);
	}

	public static void logError(Throwable t) {
		File file = ExceptionLogger.logException(t);
		logger.log(Level.SEVERE, "An exception was caught: " + t.getMessage());
		logger.log(Level.SEVERE,
				"The detailed stacktrace was saved in " + file.getName() + ", in the ChatMath/errorlogs/");
		logger.log(Level.SEVERE, "If this happens repeatedly, please contact developer.");
	}
}
