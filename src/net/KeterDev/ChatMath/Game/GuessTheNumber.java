package net.KeterDev.ChatMath.Game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.block.CommandBlock;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import net.KeterDev.ChatMath.Main;
import net.KeterDev.ChatMath.SecondExponentEngine.SEEngine;

public class GuessTheNumber {
	static int low;
	static int high;
	public static int answer;
	public static boolean active;
	static ConfigurationSection expressionSect;
	static Logger logger = Main.instance.getLogger();

	public static String getAnnouncementMessage() {
		return Main.config.getConfigurationSection("Messages").getConfigurationSection("Announcement")
				.getString("GuessTheNumber");
	}

	public static String getCorrectAnswerMessage() {
		return Main.config.getConfigurationSection("Messages").getConfigurationSection("CorrectAnswer")
				.getString("GuessTheNumber");
	}

	public static void start() {
		ConfigurationSection[] sects = Main.getGuessTheNumberSections(Main.config);

		if (sects != null) {
			if (sects.length == 0) {
				Main.server.getConsoleSender()
						.sendMessage("[ChatMath] No (enabled) guess-the-number groups found in config!");
			} else {
				Random rand = new Random();
				expressionSect = sects[rand.nextInt(sects.length)];
				ConfigurationSection limitsSect = expressionSect.getConfigurationSection("Limits");
				ConfigurationSection lowerLimitSect = limitsSect.getConfigurationSection("Lower");
				ConfigurationSection upperLimitSect = limitsSect.getConfigurationSection("Upper");
				low = SEEngine.getRandom(lowerLimitSect.getInt("From", 0), lowerLimitSect.getInt("To", 10));
				high = SEEngine.getRandom(upperLimitSect.getInt("From", 0), upperLimitSect.getInt("To", 10));
				answer = SEEngine.getRandom(low, high);

				active = true;
				Main.server.broadcastMessage(ChatColor.translateAlternateColorCodes('&',
						GuessTheNumber.parsePlaceholders(Main.chatPrefix + getAnnouncementMessage())));
			}
		}
	}

	public static boolean checkResult(String message, CommandSender sender) {
		try {
			if (active) {
				if (sender.hasPermission("chatmath.answer.guessthenumber")) {
					if (message.startsWith(String.valueOf(answer) + " ") || message.equals(String.valueOf(answer))) {
						if (sender instanceof Player) {
							Main.server.broadcastMessage(parsePlaceholders(ChatColor.translateAlternateColorCodes('&',
									Main.chatPrefix + getCorrectAnswerMessage()), (Player) sender));
						} else if (sender instanceof ConsoleCommandSender) {
							Main.server.broadcastMessage(parsePlaceholders(ChatColor.translateAlternateColorCodes('&',
									Main.chatPrefix + getCorrectAnswerMessage()), "CONSOLE"));
						} else if (sender instanceof CommandBlock) {
							Main.server.broadcastMessage(parsePlaceholders(ChatColor.translateAlternateColorCodes('&',
									Main.chatPrefix + getCorrectAnswerMessage()), "CommandBlock"));
						} else {
							Main.server.broadcastMessage(parsePlaceholders(ChatColor.translateAlternateColorCodes('&',
									Main.chatPrefix + getCorrectAnswerMessage()), "?"));
						}

						if (sender instanceof Player) {
							if (expressionSect.get("CommandsType", "order").toString().startsWith("random")) {
								Main.server.getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {

									@Override
									public void run() {
										List<String> rewardCommands = new ArrayList<>();
										for (Object rewardLine : expressionSect.getList("Commands")) {
											rewardCommands.add(
													rewardLine.toString().replaceAll("%player%", sender.getName()));
										}

										int j = 1;
										try {
											if (expressionSect.get("CommandsType", "order").toString()
													.split(":").length >= 2
													&& Integer.parseInt(expressionSect.get("CommandsType", "order")
															.toString().split(":")[1]) > 0) {
												j = Integer.parseInt(expressionSect.get("CommandsType", "order")
														.toString().split(":")[1]);
											}
										} catch (NumberFormatException e) {
											j = 1;
										}

										for (int i = 0; i < j; i++) {
											int idx = new Random().nextInt(rewardCommands.size() + 1);
											Main.server.dispatchCommand(Main.server.getConsoleSender(),
													rewardCommands.get(idx));
											rewardCommands.remove(idx);
										}
									}
								});
							} else if (expressionSect.get("CommandsType", "order").toString().startsWith("chance")) {
								Main.server.getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {

									@Override
									public void run() {
										for (Object rewardLine : expressionSect.getList("Commands")) {
											String rewardCommand = rewardLine.toString().replaceAll("%player%",
													sender.getName());
											if (rewardCommand.matches("[0-9]{1,3}:.{1,}")
													&& rollChance(Integer.parseInt(rewardCommand.split(":")[0]))) {
												Main.server.dispatchCommand(Main.server.getConsoleSender(),
														rewardCommand.split(":")[1]);
											}
										}
									}
								});
							} else {
								Main.server.getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {

									@Override
									public void run() {
										for (Object rewardLine : expressionSect.getList("Commands")) {
											String rewardCommand = rewardLine.toString().replaceAll("%player%",
													sender.getName());
											Main.server.dispatchCommand(Main.server.getConsoleSender(), rewardCommand);
										}
									}
								});
							}
						}
						active = false;
						Main.gameType = -1;
						return true;
					}
				}
			}
		} catch (Exception exc) {
			Main.server.getConsoleSender()
					.sendMessage("[ChatMath] Error reading from config, please try removing config.yml.");
			Main.logError(exc);
		}
		return false;
	}

	private static boolean rollChance(int perc) {
		return new Random().nextInt(100) <= perc;
	}

	protected static String parsePlaceholders(String text) {
		text = text.replaceAll("%low%", String.valueOf(low));
		text = text.replaceAll("%high%", String.valueOf(high));
		text = text.replaceAll("%answer%", String.valueOf(answer));
		return text;
	}

	protected static String parsePlaceholders(String text, String nick) {
		text = text.replaceAll("%playername%", nick);
		text = text.replaceAll("%displayname%", nick);
		return text;
	}

	protected static String parsePlaceholders(String text, Player player) {
		text = text.replaceAll("%answer%", String.valueOf(answer));
		text = text.replaceAll("%playername%", player.getName());
		text = text.replaceAll("%displayname%", player.getDisplayName());
		return text;
	}
}
