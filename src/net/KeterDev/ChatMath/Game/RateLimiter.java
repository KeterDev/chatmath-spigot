package net.KeterDev.ChatMath.Game;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

import net.KeterDev.ChatMath.Main;

public class RateLimiter {
	private Map<Player, Integer> ratelimitPts = new HashMap<>();
	private Map<Player, Long> ratelimited = new HashMap<>();

	public RateLimiter() {
		await();
	}

	public int addRateLimitPoint(Player player) {
		ratelimitPts.put(player, ratelimitPts.getOrDefault(player, 0) + 1);
		if (ratelimitPts.get(player) >= Main.config.getConfigurationSection("RateLimitation").getInt("Amount", 5)) {
			ratelimited.put(player, System.currentTimeMillis()
					+ (Main.config.getConfigurationSection("RateLimitation").getLong("Time", 300) * 1000));
			ratelimitPts.remove(player);
		}

		return ratelimitPts.get(player);
	}

	public long isRateLimited(Player player) {
		if (ratelimited.containsKey(player)) {
			return ratelimited.get(player);
		}
		return 0;
	}

	private void await() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					if (ratelimited.isEmpty()) {
						continue;
					}

					for (Map.Entry<Player, Long> entry : ratelimited.entrySet()) {
						if (System.currentTimeMillis() >= entry.getValue()) {
							ratelimited.remove(entry.getKey());
						}
					}
				}
			}
		});
	}
}
