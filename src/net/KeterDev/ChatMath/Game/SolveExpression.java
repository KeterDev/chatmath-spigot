package net.KeterDev.ChatMath.Game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.block.CommandBlock;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import net.KeterDev.ChatMath.Main;
import net.KeterDev.ChatMath.SecondExponentEngine.Operator;
import net.KeterDev.ChatMath.SecondExponentEngine.SEEngine;

public class SolveExpression {
	public static String exp;
	public static String result;
	public static boolean active;
	static ConfigurationSection expressionSect;
	static Logger logger = Main.instance.getLogger();

	public static String getAnnouncementMessage() {
		return Main.config.getConfigurationSection("Messages").getConfigurationSection("Announcement")
				.getString("SolveExpression");
	}

	public static String getCorrectAnswerMessage() {
		return Main.config.getConfigurationSection("Messages").getConfigurationSection("CorrectAnswer")
				.getString("SolveExpression");
	}

	public static void start() {
		try {
			ConfigurationSection[] sects = Main.getSolveExpressionSections(Main.config);

			if (sects != null) {
				if (sects.length == 0) {
					Main.server.getConsoleSender()
							.sendMessage("[ChatMath] No (enabled) expression groups found in config!");
				} else {
					Random rand = new Random();
					expressionSect = sects[rand.nextInt(sects.length)];
					ConfigurationSection lengthSect = expressionSect.getConfigurationSection("Length");
					ConfigurationSection varSect = expressionSect.getConfigurationSection("Variables");
					ConfigurationSection opSect = expressionSect.getConfigurationSection("Operands");

					Map<Operator, Boolean> operators = new HashMap<>();
					operators.put(Operator.ADDITION, opSect.getBoolean("Addition", false));
					operators.put(Operator.SUBTRACTION, opSect.getBoolean("Subtraction", false));
					operators.put(Operator.MULTIPLICATION, opSect.getBoolean("Multiplication", false));
					operators.put(Operator.DIVISION, opSect.getBoolean("Division", false));
					operators.put(Operator.EXPONENT, opSect.getBoolean("Exponent", false));

					active = true;

					logger.log(Level.FINE, "[ChatMath] Generating expression...");
					try {
						while (true) {

							exp = SEEngine.generateExpression(varSect.getInt("Lower", 0), varSect.getInt("Upper", 100),
									operators, expressionSect.getBoolean("AllowDoubleResult", false),
									SEEngine.getRandom(lengthSect.getInt("Lower", 1), lengthSect.getInt("Upper", 2)));
							result = SEEngine.calculateResult(exp);
							if (expressionSect.getLong("MaxResult", 100000) > Long.valueOf(result)
									&& Long.valueOf(result) > 0) {
								break;
							}
						}

					} catch (IllegalArgumentException e) {
						logger.log(Level.CONFIG,
								"[ChatMath] Warning: Length's Upper limit can't be greater than Lower limit.");
					}

					Main.server.broadcastMessage(ChatColor.translateAlternateColorCodes('&',
							SolveExpression.parsePlaceholders(Main.chatPrefix + getAnnouncementMessage())));
				}
			}
		} catch (Exception e) {
			Main.logError(e);
		}
	}

	public static boolean checkResult(String message, CommandSender sender) {
		// [0-9]{1,3}:.*
		try {
			if (active) {
				if (sender.hasPermission("chatmath.answer.solveexpression")) {
					if (message.startsWith(String.valueOf(result) + " ") || message.equals(String.valueOf(result))) {
						if (sender instanceof Player) {
							Main.server.broadcastMessage(parsePlaceholders(ChatColor.translateAlternateColorCodes('&',
									Main.chatPrefix + getCorrectAnswerMessage()), (Player) sender));
						} else if (sender instanceof ConsoleCommandSender) {
							Main.server.broadcastMessage(parsePlaceholders(ChatColor.translateAlternateColorCodes('&',
									Main.chatPrefix + getCorrectAnswerMessage()), "CONSOLE"));
						} else if (sender instanceof CommandBlock) {
							Main.server.broadcastMessage(parsePlaceholders(ChatColor.translateAlternateColorCodes('&',
									Main.chatPrefix + getCorrectAnswerMessage()), "CommandBlock"));
						} else {
							Main.server.broadcastMessage(parsePlaceholders(ChatColor.translateAlternateColorCodes('&',
									Main.chatPrefix + getCorrectAnswerMessage()), "?"));
						}

						if (sender instanceof Player) {
							if (expressionSect.get("CommandsType", "order").toString().startsWith("random")) {
								Main.server.getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {

									@Override
									public void run() {
										List<String> rewardCommands = new ArrayList<>();
										for (Object rewardLine : expressionSect.getList("Commands")) {
											rewardCommands.add(
													rewardLine.toString().replaceAll("%player%", sender.getName()));
										}

										int j = 1;
										try {
											if (expressionSect.get("CommandsType", "order").toString()
													.split(":").length >= 2
													&& Integer.parseInt(expressionSect.get("CommandsType", "order")
															.toString().split(":")[1]) > 0) {
												j = Integer.parseInt(expressionSect.get("CommandsType", "order")
														.toString().split(":")[1]);
											}
										} catch (NumberFormatException e) {
											j = 1;
										}

										for (int i = 0; i < j; i++) {
											try {
												int idx = new Random().nextInt(rewardCommands.size() + 1);
												Main.server.dispatchCommand(Main.server.getConsoleSender(),
														rewardCommands.get(idx));
												rewardCommands.remove(idx);
											} catch (Exception e) {
												break;
											}
										}
									}
								});
							} else if (expressionSect.get("CommandsType", "order").toString().startsWith("chance")) {
								Main.server.getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {

									@Override
									public void run() {
										for (Object rewardLine : expressionSect.getList("Commands")) {
											String rewardCommand = rewardLine.toString().replaceAll("%player%",
													sender.getName());
											try {
												if (rewardCommand.matches("[0-9]{1,3}:.{1,}")
														&& rollChance(Integer.parseInt(rewardCommand.split(":")[0]))) {
													Main.server.dispatchCommand(Main.server.getConsoleSender(),
															rewardCommand.split(":")[1]);
												}
											} catch (Exception e) {
												break;
											}
										}
									}
								});
							} else {
								Main.server.getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {

									@Override
									public void run() {
										for (Object rewardLine : expressionSect.getList("Commands")) {
											String rewardCommand = rewardLine.toString().replaceAll("%player%",
													sender.getName());
											Main.server.dispatchCommand(Main.server.getConsoleSender(), rewardCommand);
										}
									}
								});
							}
						}
						active = false;
						Main.gameType = -1;
						return true;
					}
				}
			}
		} catch (Exception exc) {
			Main.server.getConsoleSender()
					.sendMessage("[ChatMath] Error reading from config, please try removing config.yml.");
			Main.logError(exc);
		}
		return false;
	}

	private static boolean rollChance(int perc) {
		return new Random().nextInt(100) <= perc;
	}

	protected static String parsePlaceholders(String text) {
		text = text.replaceAll("%expression%", exp);
		text = text.replaceAll("%answer%", result);
		return text;
	}

	protected static String parsePlaceholders(String text, String nick) {
		text = text.replaceAll("%expression%", exp);
		text = text.replaceAll("%answer%", result);
		text = text.replaceAll("%playername%", nick);
		text = text.replaceAll("%displayname%", nick);
		return text;
	}

	protected static String parsePlaceholders(String text, Player player) {
		text = text.replaceAll("%expression%", exp);
		text = text.replaceAll("%answer%", result);
		text = text.replaceAll("%playername%", player.getName());
		text = text.replaceAll("%displayname%", player.getDisplayName());
		return text;
	}

}
