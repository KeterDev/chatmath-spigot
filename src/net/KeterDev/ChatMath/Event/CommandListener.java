package net.KeterDev.ChatMath.Event;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.logging.Level;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import net.KeterDev.ChatMath.ExceptionLogger;
import net.KeterDev.ChatMath.Main;
import net.KeterDev.ChatMath.Game.GuessTheNumber;
import net.KeterDev.ChatMath.Game.SolveExpression;

public class CommandListener implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		try {
			if (args.length >= 1) {
				if (args[0].equals("start")) {
					if (args.length == 1) {
						if (sender.hasPermission("chatmath.startnew.*")) {

							sender.sendMessage(parseColor(Main.chatPrefix
									+ "&aNote: you can specify the exact game you want to start with &a/cm start <game name> - where <game name> is SolveExpression or GuessNumber"));
							sender.sendMessage(parseColor(Main.chatPrefix + "&aStarting up a random game..."));
							Main.runGame();
						} else {
							sender.sendMessage(parseColor(Main.chatPrefix
									+ "&4You must have a permission to start any challenge to start a random one."));
						}
						return true;
					} else if (args.length >= 2) {
						if (args[1].equalsIgnoreCase("SolveExpression")) {
							if (!sender.hasPermission("chatmath.startnew.solveexpression")) {
								sender.sendMessage(parseColor(
										Main.chatPrefix + "&cYou don't have a permission to start a new challenge!"));
								return true;
							}
							sender.sendMessage(parseColor(Main.chatPrefix + "&aStarting up a SolveExpression game..."));
							Main.runGame(0);
						} else if (args[1].equalsIgnoreCase("GuessNumber")) {
							if (!sender.hasPermission("chatmath.startnew.guessthenumber")) {
								sender.sendMessage(parseColor(
										Main.chatPrefix + "&cYou don't have a permission to start a new challenge!"));
								return true;
							}
							sender.sendMessage(parseColor(Main.chatPrefix + "&aStarting up a GuessNumber game..."));
							Main.runGame(1);
						} else {
							sender.sendMessage(parseColor(Main.chatPrefix + "&cNo such game - " + args[1]));
						}
						return true;
					}
					Main.server.broadcastMessage(
							parseColor(Main.chatPrefix + "&a" + sender.getName() + " has started a new challenge."));
					return true;
				} else if (args[0].equals("answer")) {
					if (args.length >= 2) {
						if (sender instanceof Player && Main.rl.isRateLimited((Player) sender) == 0L) {
							try {
								Integer.parseInt(args[1]);
								if (Main.gameType == 0) {
									SolveExpression.checkResult(args[1], sender);
								} else if (Main.gameType == 1) {
									GuessTheNumber.checkResult(args[1], sender);
								}
							} catch (NumberFormatException e) {
								sender.sendMessage(parseColor(Main.chatPrefix + "&cAnswer can contain digits only!"));
							}
							return true;
						} else if (sender instanceof Player) {
							if (Main.rl.isRateLimited(((Player) sender)) > 0
									|| !(((Player) sender).hasPermission("chatmath.bypassratelimits")
											|| ((Player) sender).hasPermission("chatmath.bypassratelimits"))) {
								sender.sendMessage(parseColor(Main.chatPrefix + parsePlaceholders(
										Main.config.getConfigurationSection("Messages").getString("RateLimited",
												"&4You have answered to %ratelimit_amount% challenge(s) already. You can answer again in %ratelimit_time%."),
										(Player) sender)));
							}
						}

					} else if (Main.gameType == 0) {
						if (sender.hasPermission("chatmath.getanswer.solveexpression")) {
							sender.sendMessage(
									parseColor(Main.chatPrefix + "&aThe right answer is " + SolveExpression.result));
						} else {
							sender.sendMessage(
									parseColor(Main.chatPrefix + "&cYou don't have a permission to get an answer!"));
						}
					} else if (Main.gameType == 1) {
						if (sender.hasPermission("chatmath.getanswer.guessthenumber")) {
							sender.sendMessage(
									parseColor(Main.chatPrefix + "&aThe right answer is " + GuessTheNumber.answer));
						} else {
							sender.sendMessage(
									parseColor(Main.chatPrefix + "&cYou don't have a permission to get an answer!"));
						}
					} else {
						sender.sendMessage(parseColor(Main.chatPrefix + "&cChallenge is not active right now."));
					}
				} else if (args[0].equals("reload")) {
					if (sender.hasPermission("chatmath.reload")) {
						sender.sendMessage(parseColor(Main.chatPrefix + "&aReloading config.yml from disk..."));
						Main.config = Main.getReloadConfig();

						sender.sendMessage(parseColor(Main.chatPrefix + "&aRestarting challenge scheduler..."));
						Main.server.getScheduler().cancelTask(Main.schedulerTaskID);
						Main.scheduleRestart();

						sender.sendMessage(parseColor(Main.chatPrefix + "&aReload complete."));
					} else {
						sender.sendMessage(
								parseColor(Main.chatPrefix + "&cYou don't have a permission to reload plugin!"));
					}
				} else if (args[0].equals("answer")) {
					if (SolveExpression.active) {
						if (sender.hasPermission("chatmath.answer.solveexpression")) {
							if (SolveExpression.checkResult(args[1], sender) == false) {
								sender.sendMessage(parseColor(Main.chatPrefix + "&cWrong answer..."));
							}
						} else {
							sender.sendMessage(parseColor("&cYou don't have a permission to answer!"));
						}
					} else if (GuessTheNumber.active) {
						if (sender.hasPermission("chatmath.answer.guessthenumber")) {
							if (GuessTheNumber.checkResult(args[1], sender) == false) {
								sender.sendMessage(parseColor(Main.chatPrefix + "&cWrong answer..."));
							}
						} else {
							sender.sendMessage(parseColor("&cYou don't have a permission to answer!"));
						}
					} else {
						sender.sendMessage(parseColor(Main.chatPrefix + "&cChallenge is not active right now."));
					}
				}
			} else if (args[0].equals("update")) {
				if (sender instanceof ConsoleCommandSender) {
					try {
						if (Main.downloadUpdate().exists()) {
							Main.logger.log(Level.INFO, "Download complete.");
						} else {
							Main.logger.log(Level.INFO, "Download appears to be failed.");
						}
					} catch (IOException e) {
						File logFile = ExceptionLogger.logException(e);
						Main.logger.log(Level.SEVERE,
								"Download failed. A detailed stacktrace is saved to ChatMath/errorlogs/"
										+ logFile.getName());
					}
				} else {
					sender.sendMessage(
							parseColor(Main.chatPrefix + "&cThis command can be issued from the console only."));
				}
			} else {
				sender.sendMessage(parseColor("&b====== &aChat&bMath ======"));
				sender.sendMessage(parseColor("&bBy &4KeterDev"));
				sender.sendMessage(parseColor("&b/chatmath reload - reload config from disk"));
				sender.sendMessage(parseColor("&b/chatmath start [game number] - start a new math game"));
				sender.sendMessage(parseColor("&b/chatmath answer [answer] - answer the expression"));
			}
		} catch (Exception e) {
			ExceptionLogger.logException(e);
		}
		return true;
	}

	private String parsePlaceholders(String input, Player player) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Main.rl.isRateLimited(player));

		int hrs = calendar.get(Calendar.HOUR_OF_DAY);
		int min = calendar.get(Calendar.MINUTE);
		int sec = calendar.get(Calendar.SECOND);

		return input
				.replaceAll("%ratelimit_amount%",
						String.valueOf(Main.config.getConfigurationSection("RateLimitation").getInt("Amount", 5)))
				.replaceAll("%ratelimit_time%", hrs + "h " + min + "m " + sec + "s");
	}

	private String parseColor(String string) {
		return ChatColor.translateAlternateColorCodes('&', string);
	}
}
