package net.KeterDev.ChatMath.Event;

import java.util.Calendar;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import net.KeterDev.ChatMath.Main;
import net.KeterDev.ChatMath.Game.GuessTheNumber;
import net.KeterDev.ChatMath.Game.SolveExpression;

public class ChatListener implements Listener {
	ConfigurationSection messages = Main.config.getConfigurationSection("Messages");
	ConfigurationSection rateLimitSection = Main.config.getConfigurationSection("RateLimitation");

	@EventHandler
	public void onPlayerChats(AsyncPlayerChatEvent e) {
		try {
			if (Main.rl.isRateLimited(e.getPlayer()) != 0 && !(e.getPlayer().hasPermission("chatMath.bypassratelimits")
					|| e.getPlayer().hasPermission("chatmath.bypassratelimits"))) {
				String msg = parseColor(Main.chatPrefix + parsePlaceholders(messages.getString("RateLimited",
						"&4You have answered to %ratelimit_amount% challenge(s) already. You can answer again in %ratelimit_time%."),
						e.getPlayer()));
				e.getPlayer().sendMessage(msg);
				return;
			}

			if (Main.gameType == 0) {
				boolean success = SolveExpression.checkResult(e.getMessage(), e.getPlayer());
				if (success != false && Main.config.getBoolean("HideAnswerMessage", false)) {
					Main.rl.addRateLimitPoint(e.getPlayer());
					e.setCancelled(true);
				}
			} else if (Main.gameType == 1) {
				boolean success = GuessTheNumber.checkResult(e.getMessage(), e.getPlayer());
				if (success != false && Main.config.getBoolean("HideAnswerMessage", false)) {
					Main.rl.addRateLimitPoint(e.getPlayer());
					e.setCancelled(true);
				}
			}
		} catch (Exception exc) {
			Main.logError(exc);
		}
	}

	private String parseColor(String string) {
		return ChatColor.translateAlternateColorCodes('&', string);
	}

	private String parsePlaceholders(String input, Player player) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Main.rl.isRateLimited(player) - System.currentTimeMillis());

		int hrs = calendar.get(Calendar.HOUR_OF_DAY);
		int min = calendar.get(Calendar.MINUTE);
		int sec = calendar.get(Calendar.SECOND);

		input = input.replaceAll("%ratelimit_amount%",
				String.valueOf(Main.config.getConfigurationSection("RateLimitation").getInt("Amount", 5)));
		input = input.replaceAll("%ratelimit_time%", hrs + "h " + min + "m " + sec + "s");
		return input;
	}
}
