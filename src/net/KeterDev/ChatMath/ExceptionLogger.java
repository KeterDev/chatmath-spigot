package net.KeterDev.ChatMath;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExceptionLogger {
	public static File logException(Throwable t) {
		String fileName = new SimpleDateFormat("yyyyMMddHHmm'.log'").format(new Date());
		File logDir = new File(Main.instance.getDataFolder() + File.separator + "errorlogs");
		File file = new File(logDir + File.separator + fileName);
		
		file.getParentFile().mkdirs();
		try {
			file.createNewFile();
		} catch (IOException e) {
		}
		
		try (FileWriter fw = new FileWriter(file); PrintWriter pw = new PrintWriter(fw)) {
			t.printStackTrace(pw);
			return file;
		} catch (IOException e) {
		}
		return null;
	}
}
