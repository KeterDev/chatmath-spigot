package net.KeterDev.ChatMath.SecondExponentEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.script.ScriptException;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class SEEngine {
	public static String generateExpression(int minVarValue, int maxVarValue, Map<Operator, Boolean> allowedOperators,
			boolean allowDouble, int length) throws IllegalArgumentException {
		String exp = "";

		if (length < 1)
			throw new IllegalArgumentException("Length cannot be less than 1!");
		else if (minVarValue >= maxVarValue)
			throw new IllegalArgumentException("Minimum value cannot equal or be bigger than maximum value!");

		for (int i = 0; i < length; i++) {
			String currentBracket = "";
			Operator op = generateOperator(allowedOperators);

			int a = getRandom(minVarValue, maxVarValue);
			int b = getRandom(minVarValue, maxVarValue);

			if (op == Operator.DIVISION) {
				while (a <= b || a / b <= 0) {
					a = getRandom(minVarValue, maxVarValue);
					b = getRandom(minVarValue, maxVarValue);
				}
			} else if (op == Operator.EXPONENT) {
				b = getRandom(0, 4);
			}

			if (!exp.equals("")) {
				currentBracket = " (";
				exp += op.toString(op);

			} else {
				currentBracket = "(";
			}

			currentBracket += a + " ";
			currentBracket += op.toString(op) + " ";
			currentBracket += b;
			currentBracket += ") ";

			exp += currentBracket;
		}

		if (length == 1) {
			return exp.substring(1, exp.length() - 2);
		}

		return exp;
	}

	public static String calculateResult(String expression) throws ScriptException {
		Expression exp = new ExpressionBuilder(expression).build();
		return String.valueOf((long) exp.evaluate());
	}

	public static boolean checkIfPower(int input, int power) {
		while (input > (power - 1) && input % power == 0)
			input /= power;
		return input == 1;
	}

	private static Operator generateOperator(Map<Operator, Boolean> operators) throws IllegalArgumentException {
		List<Operator> operatorsArray = new ArrayList<>();
		if (operators.isEmpty()) {
			throw new IllegalArgumentException("AllowedOperators Map cannot be empty!");
		}

		for (Map.Entry<Operator, Boolean> entry : operators.entrySet()) {
			if (entry.getValue()) {
				operatorsArray.add(entry.getKey());
			}
		}

		return operatorsArray.get(new Random().nextInt(operatorsArray.size()));
	}

	public static int getRandom(int from, int to) {
		if (from < to)
			return from + new Random().nextInt(Math.abs(to - from));
		return from - new Random().nextInt(Math.abs(to - from));
	}
}
