package net.KeterDev.ChatMath.SecondExponentEngine;

public enum Operator {
	ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, EXPONENT;

	public String toString(Operator operator) {
		return operator == Operator.ADDITION ? "+"
				: operator == Operator.SUBTRACTION ? "-"
						: operator == MULTIPLICATION ? "*"
								: operator == DIVISION ? "/" : operator == Operator.EXPONENT ? "^" : "?";
	}
}
